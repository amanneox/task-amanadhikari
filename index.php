<?php require_once 'includes/config.php'; ?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<title>Task 2</title>
</head>
<body>
   <header>
      <div class="header">
         <span class="head-title">
            <h1>Exam Preparation Online</h1>
         </span>
      </div>
   </header>
   <div class="main">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-3 filter">
               <div class="filter-box">
                  <div class="row">
                     <a class="apply" href="#">Apply filter</a>
                  </div>
                  <div class="row">
                     <div class="col-sm-11">
                        <div class="form-group control1">
                           <label for="selectcontrol">Organisation:</label>
                           <select class="form-control classic" id="control1">
                              <option>ARW</option>
                              <option>2</option>
                              <option>3</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-11">
                        <div class="form-group control1">
                           <label for="selectcontrol">Engineering Stream:</label>
                           <select class="form-control classic" id="control1">
                              <option>Select</option>
                              <option>2</option>
                              <option>3</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-11">
                        <div class="form-group control1">
                           <label for="selectcontrol">Application Mode:</label>
                           <select class="form-control classic" id="control1">
                              <option>Select</option>
                              <option>2</option>
                              <option>3</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="filter-box2">
                  <div class="row">
                     <a class="apply" href="#">Clear all/Select all</a>
                  </div>
                  <br>
                  <div class="row">
                     <div class="form-check form-check-inline control2">
                        <label class="form-check-label custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Selection Procedure</span>
                        </label>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-check form-check-inline control2">
                        <label class="form-check-label custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Technical Interview</span>
                        </label>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-check form-check-inline control2">
                        <label class="form-check-label custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Analytical Questions</span>
                        </label>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-check form-check-inline control2">
                        <label class="form-check-label custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">HR Questions</span>
                        </label>
                     </div>
                  </div>
                  <div class="row">
                     <div class="form-check form-check-inline control2">
                        <label class="form-check-label custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Suggestions</span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-9">
               <h2>INTERVIEW EXPERIENCES :: ARW</h2>
               <div class="content">
                  <div class="card">
                     <div class="card-content">
                        <span>
                           <p>Job Location: <b>Chennai</b></p>
                        </span>
                        <span>
                           <p>Organization: <b>ARW</b></p>
                        </span>
                        <span>
                           <p>Application Mode: <b>On Campus</b></p>
                        </span>
                        <span>
                           <p>Selection Procedure: <b>Looking for candidates strong in web development</b></p>
                        </span>
                        <span>
                           <p>Technical Interview: <b>They ask about your recent projects and technologies</b></p>
                        </span>
                        <span>
                           <p>Suggestions: <b>Have a very clear understanding of web development</b></p>
                        </span>
                        <span>
                           <p>Shared by: <b>Radha Nair</b></p>
                        </span>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-content">
                        <span>
                           <p>Job Location: <b>Delhi</b></p>
                        </span>
                        <span>
                           <p>Organization: <b>ARW</b></p>
                        </span>
                        <span>
                           <p>Application Mode: <b>On Campus</b></p>
                        </span>
                        <span>
                           <p>Selection Procedure: <b>Looking for candidates strong in web development</b></p>
                        </span>
                        <span>
                           <p>Technical Interview: <b>They ask about your recent projects and technologies</b></p>
                        </span>
                        <span>
                           <p>Suggestions: <b>Have a very clear understanding of web development</b></p>
                        </span>
                        <span>
                           <p>Shared by: <b>Radha Nair</b></p>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>
