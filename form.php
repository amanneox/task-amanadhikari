<?php require_once 'includes/config.php'; ?>

<?php
if (isset($_POST['submit'])) {
  $text=$_REQUEST['text'];
  $radio=$_REQUEST['radio'];
  echo $drop=$_REQUEST['drop'];
  $sql="INSERT Into testdata (`text`,`radio`,`drop`) VALUES('$text','$radio','$drop')";
  mysqli_query($conn, $sql);
}
 ?>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
  <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<title>Task 1</title>
</head>
<body>
  <div class="container">
  <form action="" method="post">
  <div class="form-group">
    <label for="textcontrol">Text Field</label>
    <input required type="text" name="text" class="form-control" id="text" placeholder="Enter a text">
  </div>
    <div class="form-group">
      <label for="textcontrol">Radio Buttons</label>
  <div class="form-check form-check-inline">
  <label class="form-check-label">
    <input required class="form-check-input" type="radio" name="radio" id="inlineRadio1" value="option1"> 1
  </label>
</div>
<div class="form-check form-check-inline">
  <label class="form-check-label">
    <input required class="form-check-input" type="radio" name="radio" id="inlineRadio2" value="option2"> 2
  </label>
</div>
<div class="form-check form-check-inline disabled">
  <label class="form-check-label">
    <input required class="form-check-input" type="radio" name="radio" id="inlineRadio3" value="option3" > 3
  </label>
</div>
</div>
  <div class="form-group">
    <label for="selectcontrol">DropDown Select</label>
    <select required name="drop" class="form-control" id="ControlSelect1">
      <option>1</opt(ion>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
<input name="submit" class="btn btn-primary" type="submit">
</form>
</div>

</body>
